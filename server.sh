#!/bin/bash

uvicorn app:app --app-dir src --host 0.0.0.0 --port ${SERVER_PORT:-8000}
