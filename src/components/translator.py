from transformers import pipeline


ES_EN_MODEL = "Helsinki-NLP/opus-mt-es-en"
EN_ES_MODEL = "Helsinki-NLP/opus-mt-en-es"

class Translator:

  def __init__(self) -> None:
    self.es_en_pipeline = pipeline(model=ES_EN_MODEL)
    self.en_es_pipeline = pipeline(model=ES_EN_MODEL)
  
  def translate_es_en(self, str):
    return self.es_en_pipeline(str)

  def translate_en_es(self, str):
    return self.en_es_pipeline(str)
