import time


class ClockWatch:

  def __init__(self) -> None:
    self.reset()
    print("Start time: %f" % self.start)

  def reset(self):
    self.start = time.time()

  def check(self):
    current = time.time()
    elapsed = current - self.start
    print("Elapsed time: %f" % elapsed)

  def check_and_reset(self):
    self.check()
    self.reset()
