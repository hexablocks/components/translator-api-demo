from transformers import pipeline
from clockwatch import ClockWatch


cw = ClockWatch()
gen = pipeline(model="Helsinki-NLP/opus-mt-es-en")
cw.check_and_reset()

print(gen("Primer mensaje midiendo el tiempo"))
cw.check_and_reset()

print(gen("Segundo mensaje midiendo el tiempo"))
cw.check_and_reset()

print(gen("Tercer mensaje midiendo el tiempo"))
cw.check_and_reset()
