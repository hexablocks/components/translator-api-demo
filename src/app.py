from components.translator import Translator
from fastapi import FastAPI
from pydantic import BaseModel


app = FastAPI()
t = Translator()


class TranslationRequest(BaseModel):
  src: str


@app.post("/translate/es-en")
def translate_es_en(tr: TranslationRequest):
  return {
    "src": tr.src,
    "result": t.translate_es_en(tr.src)[0]["translation_text"]
  }

@app.post("/translate/en-es")
def translate_es_en(tr: TranslationRequest):
  return {
    "src": tr.src,
    "result": t.translate_en_es(tr.src)[0]["translation_text"]
  }
