# from pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime
# from pytorch/pytorch:1.12.1-cuda11.3-cudnn8-runtime
from registry.gitlab.com/hexablocks/components/hf-transformers-pretrained:helsinkinlp-es-en-es

run pip install fastapi uvicorn

workdir /app
add . /app

entrypoint ["bash", "server.sh"]

# docker build -t local .
# docker run --rm -it local
